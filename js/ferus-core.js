/**
 * Main JS file for ferus-core
 * Author: Kael Steinert
 */
jQuery(document).ready(function ($) {

    /**
     * Countdown Timer
     */
    // Set the date
    /*var countDownDate = new Date('Oct 15, 2017 15:30:00').getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Today's Date
        var now = new Date().getTime();

        // Get count down time
        var distance = countDownDate - now;

        // Time calculations
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="countdown"
        document.getElementById("countdown").innerHTML = days + ' Days ' + hours + ' Hours ' + minutes + ' minutes ' + seconds + ' seconds';

        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("countdown").innerHTML = "Countdown Complete!";
        }
    }, 1000);*/

    /**
     * Lightbox
     */
    // $('.lb-gallery').featherlightGallery();

    /**
     * Header Search
     * @type {*}
     */
    /*var search_holder = $('.header-search');
    if (search_holder) {
        var searchTrigger = search_holder.find('label[for="search_text"]');
        $(searchTrigger).click(function(){
            if (search_holder.hasClass('active')) {
                search_holder.removeClass('active');
            } else {
                search_holder.addClass('active');
            }
        });
    }*/
    /**
     * Homepage Slider
     */
    /*$('.home_slider').slick({
        infinite: true,
        dots: true,
        arrows: false,
        autoplay: true,
        speed: 1000,
        autoplaySpeed: 4000,
        fade: true,
    });*/
    /**
     * Testimonials Slider
     */
    /*$('.testimonial_slider').slick({
        infinite: true,
        dots: false,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 12000,
    });*/
    /**
     * Gallery Slideset
     */
    /*$('.gallery-slideset').slick({
        dots: false,
        arrows: true,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });*/
    /**
     * Accordion
     */
    var allPanels = $('.accordion > [data-accordion=content]').hide();
    var activePanel = $('.accordion > [data-accordion=content].active').show();
    $('.accordion > [data-accordion=title]').click(function () {
        var target = $(this).next();
        var holder = $(this).closest('.accordion');
        var allPanelTitles = holder.find('> [data-accordion=title]');
        var allPanels = holder.find('[data-accordion=content]');

        if (!$(this).hasClass('active')) {
            allPanelTitles.removeClass('active');
            $(this).addClass('active');
        } else if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        }
        if (!target.hasClass('active')) {
            allPanels.removeClass('active').slideUp();
            target.addClass('active').slideDown();
        } else if (target.hasClass('active')) {
            target.removeClass('active').slideUp();
        }
        return false;
    });

    /**
     * Cookie Enabled Modal
     */
    // Set Cookie
    function setCookie(cname, cvalue, exdays, path) {
        var d = new Date();
        if (path){
            var path = path;
        } else {
            var path = "";
        }
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/" + path;
    }
    // Get Cookie
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    // Cookie Enabled Modal
    function cookiePop(){
        var modalSession = getCookie("modalSession");
        var url = window.location.href;
        if(url.indexOf('?welcome=1') != -1) {
            // Query string trigger
            $('#popupModal').modal('show');
        } else {
            if (modalSession != "") {
                // Cookie Found, Don't Trigger Modal
                return false;
            } else {
                // Cookie NOT Found
                //Trigger Modal
                setTimeout(function(){
                    $('#popupModal').modal('show');
                }, 1500);
                // Create Cookie
                setCookie("modalSession", "value", 30, "");
            }
        }
    }cookiePop();

});// END document.ready

/*************************************************************************/
/* Window.load  */
/*************************************************************************/
jQuery(window).load(function () {

    /**
     * Smooth Scroll
     */
    jQuery('header .menu-item a[href^="#"]:not([href="#"])').on('click', function (e) {
        e.preventDefault();
        var target = this.hash;
        var $target = jQuery(target);
        if (jQuery(window).width() > 767 && jQuery(window).width() < 991) {
            var $offset = $target.offset().top - 225;
        }
        else {
            var $offset = $target.offset().top - 180;
        }
        jQuery('html, body').stop().animate({
            'scrollTop': $offset
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    });

});/* END window.load */

/**
 * Match Height Columns
 * USAGE: data-col=a
 */
function colMatchHeight() {
    var cols = document.querySelectorAll('[data-col]'),
        encountered = [];
    for (i = 0; i < cols.length; i++) {
        var attr = cols[i].getAttribute('data-col');
        if (encountered.indexOf(attr) == -1) {
            encountered.push(attr);
        }
    }
    for (set = 0; set < encountered.length; set++) {
        var col = document.querySelectorAll('[data-col="' + encountered[set] + '"]'),
            group = [];
        for (i = 0; i < col.length; i++) {
            col[i].style.height = 'auto';
            group.push(col[i].scrollHeight);
        }
        for (i = 0; i < col.length; i++) {
            col[i].style.height = Math.max.apply(Math, group) + 'px';
        }
    }
}
window.addEventListener("load", colMatchHeight);
window.addEventListener("resize", colMatchHeight);

/**
 * Proper Parallax
 */
function getTop(elem) {
    var box = elem.getBoundingClientRect();
    var body = document.body;
    var docEl = document.documentElement;
    var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
    var clientTop = docEl.clientTop || body.clientTop || 0;
    var top  = box.top +  scrollTop - clientTop;
    return Math.round(top);
}
function parallaxImages() {
    // Set the scroll for each parallax individually
    var plx = document.getElementsByClassName('parallax');
    for(i=0;i<plx.length;i++){
        var height = plx[i].clientHeight;
        var img = plx[i].getAttribute('data-plx-img');
        var plxImg = document.createElement("div");
        plxImg.className += " plx-img";
        plxImg.style.height = (height+(height/2))+'px';
        plxImg.style.backgroundImage = 'url('+ img +')';
        plx[i].insertBefore(plxImg, plx[i].firstChild);
    }
}
window.addEventListener('load', parallaxImages);
function plxScroll(){
    var scrolled = window.scrollY;
    var win_height_padded = window.innerHeight * 1.25;
    // Set the scroll for each parallax individually
    var plx = document.getElementsByClassName('parallax');
    for(i=0;i<plx.length;i++){
        var offsetTop = getTop(plx[i]);
        //var orientation = plx[i].getAttribute('data-plx-o');
        if (scrolled + win_height_padded >= offsetTop) {
            var plxImg = plx[i].getElementsByClassName('plx-img')[0];
            if(plxImg) {
                var plxImgHeight = plxImg.clientHeight;
                var singleScroll = (scrolled - offsetTop) - plxImgHeight/5;
                plxImg.style.top = (singleScroll / 5) + "px";
            }
        }
    }
}
window.addEventListener('load', plxScroll);
window.addEventListener('resize', plxScroll);
window.addEventListener('scroll', plxScroll);

/**
 * Scroll Revealing Items
 */
/*var isScrolling = false;
window.addEventListener("scroll", throttleScroll);
function throttleScroll(e) {
    if (isScrolling == false) {
        window.requestAnimationFrame(function() {
            scrolling(e);
            isScrolling = false;
        });
    }
    isScrolling = true;
}
document.addEventListener("DOMContentLoaded", scrolling, false);

function scrolling(e) {
    var scrollItems = document.querySelectorAll("[data-reveal]");
    for (var i = 0; i < scrollItems.length; i++) {
        var scrollItem = scrollItems[i];
        var animationType = scrollItems[i].getAttribute('data-reveal');

        if (isPartiallyVisible(scrollItem)) {
            scrollItem.classList.add('animated', animationType);
        } //else {
            //scrollItem.classList.remove('animated', animationType);
        //}
    }
}
function isPartiallyVisible(el) {
    var elementBoundary = el.getBoundingClientRect();
    var top = elementBoundary.top;
    var bottom = elementBoundary.bottom;
    var height = elementBoundary.height;

    return ((top + height >= 0) && (height + window.innerHeight >= bottom));
}
function isFullyVisible(el) {
    var elementBoundary = el.getBoundingClientRect();
    var top = elementBoundary.top;
    var bottom = elementBoundary.bottom;

    return ((top >= 0) && (bottom <= window.innerHeight));
}*/